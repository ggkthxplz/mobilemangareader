/*
 * ComicsReader is an Android application to read comics
 * Copyright (C) 2011-2013 Cedric OCHS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package net.kervala.comicsreader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import android.net.Uri;

public class MangableAlbumsIndexHandler extends DefaultHandler {
	private ArrayList<ThumbnailItem> mItems;
	private String mTitle;
	private int mSize = 0;
	private String mFilename;
	private String mThumbnail;
	private String mUrl;
	private StringBuilder mBuilder = new StringBuilder();
	
	public MangableAlbumsIndexHandler() {
		mItems = new ArrayList<ThumbnailItem>();
	}
	
	public ArrayList<ThumbnailItem> getItems() {
		return mItems;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		// reset previous values
		if ("item".equals(localName)) {
			mTitle = null;
			mFilename = null;
			mUrl = null;
			mThumbnail = null;
			mSize = 0;
		}
		mBuilder.setLength(0);
	}
	
	private boolean isAlbumValid() {
		if (mFilename == null || "".equals(mFilename)) return false;
		if (mSize <= 0) return false;
		if (mUrl == null || !"http".equals(Uri.parse(mUrl).getScheme())) return false;
		if (mThumbnail == null || !"http".equals(Uri.parse(mThumbnail).getScheme())) return false;

		return true;
	}
	
	@Override
	public void endElement(String namespaceURI, String localName, String qName) {
		String str = mBuilder.toString().trim();
		mFilename = "a";
		mSize = 100;
		mThumbnail = "http://2.bp.blogspot.com/-y3aVIA6OA2o/UAAdEiideGI/AAAAAAAAESE/IXvXtWf_K4E/716701897i66442.jpg";
		if ("title".equals(localName)) {
			mTitle = str;
		} else if ("link".equals(localName)) {
			mUrl = str;
		} else if ("item".equals(localName)) {
			// add album to list
//			if (isAlbumValid()) {
				BrowserItem item = new BrowserItem(mTitle, BrowserItem.TYPE_FILE, true);
				item.setAlbumUrl(mUrl);
				item.setThumbnailUrl(mThumbnail);
				item.setFilename(mFilename);
				item.setSize(mSize);
				item.setMangaName(mTitle);
				
//				try {
//				
//				// connect manga url
//				Document doc = Jsoup.connect(mUrl).get();
//				
//				// get chapters
//				Elements els = doc.select("#newlist a");
//				
////											//for each chapters, get chapter name, url
////											for(Element el: els) {
////												//get chapter name
////												String chaptName = el.select(".audi").text();
////												String chaptUrl = el.attr("href");
////												
////												//connect to the chapter
////												Document chaptDoc = Jsoup.connect(chaptUrl).get();
////												int imgCount = Integer.parseInt(chaptDoc.select("#select_page select option:last-child").text().trim());
////												
////												// get img links
////												List<String> chaptImgs = new ArrayList<String>();
////												for(int i=0; i<imgCount; i++) {
////													try {
////													String chaptSprLink = chaptUrl + i;
////													Document chapSprDoc = Jsoup.connect(chaptSprLink).get();
////													String chaptImg = chapSprDoc.select("#image").attr("src");
////													chaptImgs.add(chaptImg);
////													} catch(Exception e) {}
////												}
////												item.getChapters().put(chaptName, chaptImgs);
////											}
////											
//				String[] tmp = mUrl.split("/");
//				String realMangaName = tmp[tmp.length-1];
//				int chaptSize = els.size();
//				Element firstChapt = els.first();
//				try {
//					Document chaptDoc = Jsoup.connect(firstChapt.attr("href")).get();
//					String chaptName = chaptDoc.select("#breadcrumbs li:last-child").text();
//					int imgCount = Integer.parseInt(chaptDoc.select("#select_page select option:last-child").text().trim());
//					List<String> chaptImgs = new ArrayList<String>();
//					for(int chapIdx = 0; chapIdx < chaptSize; chapIdx ++) {
//						for(int imgIdx=0; imgIdx<imgCount; imgIdx++) {
//							String img = "http://mangable.com/files/images/"+realMangaName+"/"+chapIdx+"/"+imgIdx+".jpg";
//							chaptImgs.add(img);
//						}
//						item.getChapters().put(chaptName, chaptImgs);
//					}
//				}catch(Exception e) {
//					e.printStackTrace();
//				}
//				
//				} catch(IOException e) {
//					e.printStackTrace();
//				}
				
				
				mItems.add(item);
//			}
		}
	}

	@Override
	public void characters(char ch[], int start, int length) {
		mBuilder.append(ch, start, length);
	}
}
